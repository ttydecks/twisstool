from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='twisstool',
      version='1.2',
      description='tool for loading any MADX table files like twiss tables etc..',
      long_description=long_description,
      long_description_content_type="text/markdown",
      author='ttydecks / CERN BE-OP-LHC',
      author_email='tobias.tydecks@cern.ch',
      url='https://gitlab.cern.ch/ttydecks/twisstool',
      packages=['twisstool'],
      classifiers=[
          "Programming Language :: Python :: 3",
          "Operating System :: OS Independent"]
      )
